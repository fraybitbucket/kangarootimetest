angular.
  module('kangaroo').
  constant('KTConstant', {
   'BASE_URL': 'https://www.kangarootime.com', //PROD
   //'BASE_URL': 'http://kangarootime.us', //STAGING
   // 'BASE_URL': 'http://demo.kangarootime.com', 	//DEMO,
   'APP_VERSION': '1.4.7'
  }
);
